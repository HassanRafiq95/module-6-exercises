public class DateConverter {

    public static void main(String[] args) {
        DateConverter dateConverter = new DateConverter();
        //System.out.println(dateConverter.dateConverter("fsdfsdfdf"));
        dateConverter.arrayTester(dateConverter.monthsB);

    }

    // Initialise test array set A
    String[] monthsA = { "January", "February", "March", "April", "May", "June ",
            "July", "August", "September", "October", "November", "December" };

    // Initialise test array set B

    String[] monthsB = { "Jan", "february", "march", "apr", "May", "June ",
            "July", "august", "Sep", "oct", "November", "December" };


    public void arrayTester(String[] array)
    {
     for (String month : array)
     {
         System.out.println(dateConverter(month));
     }
    }



    public int dateConverter(String month) {
        String lowerCase = month.toLowerCase();
        String trimMonth = lowerCase.trim();

        switch (trimMonth) {

            case ("january"):
            case ("jan"):
                return 1;

            case ("february"):
            case ("feb"):
                return 2;

            case ("march"):
            case ("mar"):
                return 3;

            case ("april"):
            case ("apr"):
                return 4;

            case ("may"):
                return 5;

            case ("june"):
            case ("jun"):
                return 6;

            case ("july"):
            case ("jul"):
                return 7;

            case ("august"):
            case ("aug"):
                return 8;

            case ("september"):
            case ("sep"):
                return 9;

            case ("october"):
            case ("oct"):
                return 10;

            case ("november"):
            case ("nov"):
                return 11;

            case ("december"):
            case ("dec"):
                return 12;

            default:
                System.out.println("You have entered an invalid month, enter either the full-name or 3 letter abbreviation");
                return 0;


        }
    }
}

